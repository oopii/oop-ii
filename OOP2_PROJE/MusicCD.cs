﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_PROJE
{
    class MusicCD : Product
    {
        public string ID { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Price { get => price; set => price = value; }
        private string singer;
        public string Singer { get => singer; set => singer = value; }
        public MusicCD()
        {

        }
        public enum typeOfMusicCD
        {
            Romance,
            Hard_Rock,
            Country
        }
    }
}
