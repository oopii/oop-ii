﻿namespace OOP2_PROJE
{
    partial class FormKayitOl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSifre = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblKullaniciAdi = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblAdres = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblisim = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnKayitOlOnay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblSifre
            // 
            this.lblSifre.AutoSize = true;
            this.lblSifre.Location = new System.Drawing.Point(36, 139);
            this.lblSifre.Name = "lblSifre";
            this.lblSifre.Size = new System.Drawing.Size(35, 17);
            this.lblSifre.TabIndex = 21;
            this.lblSifre.Text = "sifre";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(122, 136);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(178, 22);
            this.txtPassword.TabIndex = 20;
            // 
            // lblKullaniciAdi
            // 
            this.lblKullaniciAdi.AutoSize = true;
            this.lblKullaniciAdi.Location = new System.Drawing.Point(36, 111);
            this.lblKullaniciAdi.Name = "lblKullaniciAdi";
            this.lblKullaniciAdi.Size = new System.Drawing.Size(81, 17);
            this.lblKullaniciAdi.TabIndex = 19;
            this.lblKullaniciAdi.Text = "kullanıcı adı";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(122, 108);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(178, 22);
            this.txtUserName.TabIndex = 18;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(36, 83);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(47, 17);
            this.lblEmail.TabIndex = 17;
            this.lblEmail.Text = "E-mail";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(122, 80);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(178, 22);
            this.txtEmail.TabIndex = 16;
            // 
            // lblAdres
            // 
            this.lblAdres.AutoSize = true;
            this.lblAdres.Location = new System.Drawing.Point(36, 55);
            this.lblAdres.Name = "lblAdres";
            this.lblAdres.Size = new System.Drawing.Size(44, 17);
            this.lblAdres.TabIndex = 15;
            this.lblAdres.Text = "adres";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(122, 52);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(178, 22);
            this.txtAddress.TabIndex = 14;
            // 
            // lblisim
            // 
            this.lblisim.AutoSize = true;
            this.lblisim.Location = new System.Drawing.Point(36, 27);
            this.lblisim.Name = "lblisim";
            this.lblisim.Size = new System.Drawing.Size(32, 17);
            this.lblisim.TabIndex = 13;
            this.lblisim.Text = "isim";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(122, 24);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(178, 22);
            this.txtName.TabIndex = 12;
            // 
            // btnKayitOlOnay
            // 
            this.btnKayitOlOnay.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnKayitOlOnay.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold);
            this.btnKayitOlOnay.Location = new System.Drawing.Point(141, 202);
            this.btnKayitOlOnay.Margin = new System.Windows.Forms.Padding(4);
            this.btnKayitOlOnay.Name = "btnKayitOlOnay";
            this.btnKayitOlOnay.Size = new System.Drawing.Size(121, 28);
            this.btnKayitOlOnay.TabIndex = 11;
            this.btnKayitOlOnay.Text = "KAYIT OL";
            this.btnKayitOlOnay.UseVisualStyleBackColor = false;
            this.btnKayitOlOnay.Click += new System.EventHandler(this.btnKayitOlOnay_Click);
            // 
            // FormKayitOl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 299);
            this.Controls.Add(this.lblSifre);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblKullaniciAdi);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lblAdres);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lblisim);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.btnKayitOlOnay);
            this.Name = "FormKayitOl";
            this.Text = "FormKayitOl";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSifre;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblKullaniciAdi;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblAdres;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblisim;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnKayitOlOnay;
    }
}