﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OOP2_PROJE
{
    public partial class FormUyeGirisi : Form
    {
        SqlCommand komut = null;
        SqlConnection bagla = new SqlConnection("Data Source=.;Initial Catalog=BookStore;Integrated Security=true");
        Form1 form1_obj = new Form1();
        Customer customerNesnesi = Customer.customer;

        public FormUyeGirisi()
        {
            InitializeComponent();
        }

        private void btnUyeGirisiOnay_Click(object sender, EventArgs e)
        {
            if (GirisYapanMusteri() == true)
            {
                form1_obj.linklblUyeGirisi.Visible = false;
                form1_obj.linklblKayitOl.Visible = false;
                MessageBox.Show("Giriş Yapıldı", "Close Window", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
                form1_obj.Show();
            }
        }
        public bool GirisYapanMusteri()
        {
            bagla.Open();
            komut = new SqlCommand("SELECT * FROM TableCustomer WHERE customerUserName='" + txtKullaniciAdiUyeGirisi.Text + "'", bagla);
            SqlDataReader veriOku = komut.ExecuteReader();
            if (veriOku.Read())
            {
                string okunanKullaniciAdi1= veriOku["customerUserName"].ToString();

                bagla.Close();
                bagla.Open();
                komut = new SqlCommand("SELECT * FROM TableCustomer WHERE customerPassword='" + txtSifreUyeGirisi.Text + "'", bagla);
                veriOku = komut.ExecuteReader();
                if (veriOku.Read())
                {
                    string okunanKullaniciAdi2 = veriOku["customerUserName"].ToString();
                    if(okunanKullaniciAdi1==okunanKullaniciAdi2)
                    {
                        customerNesnesi.ID = veriOku["customerID"].ToString();
                        customerNesnesi.name = veriOku["customerName"].ToString();
                        customerNesnesi.Email = veriOku["customerEmail"].ToString();
                        customerNesnesi.Address= veriOku["customerAddress"].ToString();
                        customerNesnesi.userName = veriOku["customerUserName"].ToString();
                        customerNesnesi.Password = veriOku["customerPassword"].ToString();
                        bagla.Close();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("geçersiz şifre");
                        bagla.Close();
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("geçersiz şifre");
                    bagla.Close();
                    return false;
                }

            }
            else
            {
                MessageBox.Show("Geçersiz Kullanıcı Adı", "Close Window", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            bagla.Close();
            return false;
        }
        private Customer GetCustomerNesnesi()
        {
            return customerNesnesi;
        }
    }
}
