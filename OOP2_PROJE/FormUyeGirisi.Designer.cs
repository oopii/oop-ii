﻿namespace OOP2_PROJE
{
    partial class FormUyeGirisi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSifreUyeGirisi = new System.Windows.Forms.Label();
            this.txtSifreUyeGirisi = new System.Windows.Forms.TextBox();
            this.btnUyeGirisiOnay = new System.Windows.Forms.Button();
            this.lblKullaniciAdiUyeGirisi = new System.Windows.Forms.Label();
            this.txtKullaniciAdiUyeGirisi = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSifreUyeGirisi
            // 
            this.lblSifreUyeGirisi.AutoSize = true;
            this.lblSifreUyeGirisi.Location = new System.Drawing.Point(84, 75);
            this.lblSifreUyeGirisi.Name = "lblSifreUyeGirisi";
            this.lblSifreUyeGirisi.Size = new System.Drawing.Size(37, 17);
            this.lblSifreUyeGirisi.TabIndex = 14;
            this.lblSifreUyeGirisi.Text = "Sifre";
            // 
            // txtSifreUyeGirisi
            // 
            this.txtSifreUyeGirisi.Location = new System.Drawing.Point(178, 75);
            this.txtSifreUyeGirisi.Name = "txtSifreUyeGirisi";
            this.txtSifreUyeGirisi.Size = new System.Drawing.Size(223, 22);
            this.txtSifreUyeGirisi.TabIndex = 13;
            // 
            // btnUyeGirisiOnay
            // 
            this.btnUyeGirisiOnay.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnUyeGirisiOnay.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold);
            this.btnUyeGirisiOnay.Location = new System.Drawing.Point(228, 119);
            this.btnUyeGirisiOnay.Name = "btnUyeGirisiOnay";
            this.btnUyeGirisiOnay.Size = new System.Drawing.Size(121, 28);
            this.btnUyeGirisiOnay.TabIndex = 12;
            this.btnUyeGirisiOnay.Text = "Giris";
            this.btnUyeGirisiOnay.UseVisualStyleBackColor = false;
            this.btnUyeGirisiOnay.Click += new System.EventHandler(this.btnUyeGirisiOnay_Click);
            // 
            // lblKullaniciAdiUyeGirisi
            // 
            this.lblKullaniciAdiUyeGirisi.AutoSize = true;
            this.lblKullaniciAdiUyeGirisi.Location = new System.Drawing.Point(84, 36);
            this.lblKullaniciAdiUyeGirisi.Name = "lblKullaniciAdiUyeGirisi";
            this.lblKullaniciAdiUyeGirisi.Size = new System.Drawing.Size(88, 17);
            this.lblKullaniciAdiUyeGirisi.TabIndex = 11;
            this.lblKullaniciAdiUyeGirisi.Text = "Kullanıcı Adı ";
            // 
            // txtKullaniciAdiUyeGirisi
            // 
            this.txtKullaniciAdiUyeGirisi.Location = new System.Drawing.Point(178, 36);
            this.txtKullaniciAdiUyeGirisi.Name = "txtKullaniciAdiUyeGirisi";
            this.txtKullaniciAdiUyeGirisi.Size = new System.Drawing.Size(223, 22);
            this.txtKullaniciAdiUyeGirisi.TabIndex = 10;
            // 
            // FormUyeGirisi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 213);
            this.Controls.Add(this.lblSifreUyeGirisi);
            this.Controls.Add(this.txtSifreUyeGirisi);
            this.Controls.Add(this.btnUyeGirisiOnay);
            this.Controls.Add(this.lblKullaniciAdiUyeGirisi);
            this.Controls.Add(this.txtKullaniciAdiUyeGirisi);
            this.Name = "FormUyeGirisi";
            this.Text = "FormUyeGirisi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSifreUyeGirisi;
        private System.Windows.Forms.TextBox txtSifreUyeGirisi;
        private System.Windows.Forms.Button btnUyeGirisiOnay;
        private System.Windows.Forms.Label lblKullaniciAdiUyeGirisi;
        private System.Windows.Forms.TextBox txtKullaniciAdiUyeGirisi;
    }
}