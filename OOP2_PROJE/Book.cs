﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP2_PROJE
{
    class Book:Product
    {
        public string ID { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Price { get => price; set => price = value; }

        private string ısbnNumber;
        private string author;
        private string publisher;
        private int page;
        private Image cover_page_picture;
        public string IsbnNumber { get => ısbnNumber; set => ısbnNumber = value; }
        public string Author { get => author; set => author = value; }
        public int Page { get => page; set => page = value; }
        public string Publisher { get => publisher; set => publisher = value; }
        public Image Cover_page_picture { get => cover_page_picture; set => cover_page_picture = value; }
        
        public Book()
        {

        }
    }
}
