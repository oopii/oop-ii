﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace OOP2_PROJE
{
    class SQLdergiCek:SQL_Veri_Cek
    {
        private SqlConnection bagla = new SqlConnection("Data Source=.;Initial Catalog=BookStore;Integrated Security=true");
        SqlCommand komut;
        SqlDataReader veriOku;

        public SqlDataReader VeriOku { get => veriOku; set => veriOku = value; }
        public SqlCommand Komut { get => komut; set => komut = value; }
        public SqlConnection Bagla { get => bagla; set => bagla = value; }

        Magazine magazineNesnesi = new Magazine();
        FormAdmin formAdmin_obj = new FormAdmin();
        FormUrünler formUrünler_obj = new FormUrünler();


        public void dergileriSQLdenCek()
        {
            bagla.Open();
            komut = new SqlCommand("SELECT magazineID,magazineName,magazinePrice,magazineIssue FROM TableMagazine", Bagla);
            VeriOku = komut.ExecuteReader();
            List<Magazine> dergiListesi = new List<Magazine>();
            while (VeriOku.Read())
            {
                Magazine nesneTut = new Magazine();
                magazineNesnesi.ID = VeriOku["magazineID"].ToString();
                magazineNesnesi.name = VeriOku["magazineName"].ToString();
                magazineNesnesi.price = double.Parse(VeriOku["magazinePrice"].ToString());
                magazineNesnesi.Issue = VeriOku["magazineIssue"].ToString();

                nesneTut.ID = magazineNesnesi.id;
                nesneTut.name = magazineNesnesi.name;
                nesneTut.price = magazineNesnesi.price;
                nesneTut.Issue = magazineNesnesi.Issue;

                dergiListesi.Add(nesneTut);
            }
            formUrünler_obj.Show();
            bagla.Close();
            formUrünler_obj.dergileriGoster(dergiListesi);
        }
    }
}
