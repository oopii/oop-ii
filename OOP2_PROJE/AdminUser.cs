﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_PROJE
{
    class AdminUser:User
    {
        private static AdminUser singletonNesnesi = null;
        public string ID { get => ID; set => ID = value; }
        public String Name { get => name; set => name = value; }
        public String Address { get => address; set => address = value; }
        public String Email { get => email; set => email = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Password { get => password; set => password = value; }
        
        private AdminUser()
        {

        }
        public static AdminUser adminUser
        {
            get
            {
                if (singletonNesnesi == null)
                {
                    singletonNesnesi = new AdminUser();
                }
                return singletonNesnesi;
            }
        }
        public void addCustomer()
        {

        }
        public void addNewBook()
        {

        }
        public void addNewMagaxine()
        {

        }
        public void addNewMusicCD()
        {

        }
    }
}
