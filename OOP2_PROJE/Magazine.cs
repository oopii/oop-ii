﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_PROJE
{
    class Magazine:Product
    {
        public string ID { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Price { get => price; set => price = value; }
        private string issue;
        public string Issue { get => issue; set => issue = value; }

        public Magazine()
        {

        }
        public enum typeOfMagazine
        {
            Actual,
            News,
            Sport,
            computer,
            technology
        }
    }
}
