﻿namespace OOP2_PROJE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtArama = new System.Windows.Forms.TextBox();
            this.pboxSepet = new System.Windows.Forms.PictureBox();
            this.pboxKampanya = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmbKitap = new System.Windows.Forms.ComboBox();
            this.cmbMagazin = new System.Windows.Forms.ComboBox();
            this.cmbCd = new System.Windows.Forms.ComboBox();
            this.cmbİndirim = new System.Windows.Forms.ComboBox();
            this.pboxUstPanel = new System.Windows.Forms.PictureBox();
            this.linklblUyeGirisi = new System.Windows.Forms.LinkLabel();
            this.linklblKayitOl = new System.Windows.Forms.LinkLabel();
            this.pboxBayrak = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblFiyat = new System.Windows.Forms.Label();
            this.lblBilgi = new System.Windows.Forms.Label();
            this.pboxUrun1 = new System.Windows.Forms.PictureBox();
            this.pboxAra = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pboxUrun2 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pboxUrun3 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pboxUrun4 = new System.Windows.Forms.PictureBox();
            this.pboxCocuk = new System.Windows.Forms.PictureBox();
            this.pboxKullaniciResim = new System.Windows.Forms.PictureBox();
            this.btnKuponlarım = new System.Windows.Forms.Button();
            this.btnKartlarım = new System.Windows.Forms.Button();
            this.btnSiparislerim = new System.Windows.Forms.Button();
            this.btnProfilim = new System.Windows.Forms.Button();
            this.pboxCizgiRoman = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pboxUrun0 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.linklblCikisYap = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pboxSepet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxKampanya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUstPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBayrak)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxAra)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun3)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCocuk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxKullaniciResim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCizgiRoman)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtArama
            // 
            this.txtArama.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtArama.Location = new System.Drawing.Point(961, 82);
            this.txtArama.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtArama.Multiline = true;
            this.txtArama.Name = "txtArama";
            this.txtArama.Size = new System.Drawing.Size(393, 41);
            this.txtArama.TabIndex = 63;
            this.txtArama.Enter += new System.EventHandler(this.txtArama_Enter);
            // 
            // pboxSepet
            // 
            this.pboxSepet.Image = ((System.Drawing.Image)(resources.GetObject("pboxSepet.Image")));
            this.pboxSepet.Location = new System.Drawing.Point(1437, 70);
            this.pboxSepet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pboxSepet.Name = "pboxSepet";
            this.pboxSepet.Size = new System.Drawing.Size(85, 54);
            this.pboxSepet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pboxSepet.TabIndex = 67;
            this.pboxSepet.TabStop = false;
            this.pboxSepet.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxSepet_MouseClick);
            // 
            // pboxKampanya
            // 
            this.pboxKampanya.Image = ((System.Drawing.Image)(resources.GetObject("pboxKampanya.Image")));
            this.pboxKampanya.Location = new System.Drawing.Point(216, 402);
            this.pboxKampanya.Margin = new System.Windows.Forms.Padding(4);
            this.pboxKampanya.Name = "pboxKampanya";
            this.pboxKampanya.Size = new System.Drawing.Size(683, 348);
            this.pboxKampanya.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxKampanya.TabIndex = 70;
            this.pboxKampanya.TabStop = false;
            this.pboxKampanya.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxKampanya_MouseClick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmbKitap
            // 
            this.cmbKitap.AccessibleDescription = "";
            this.cmbKitap.AccessibleName = "";
            this.cmbKitap.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbKitap.ForeColor = System.Drawing.Color.Maroon;
            this.cmbKitap.FormattingEnabled = true;
            this.cmbKitap.Items.AddRange(new object[] {
            "tür",
            "tür",
            "tür"});
            this.cmbKitap.Location = new System.Drawing.Point(216, 82);
            this.cmbKitap.Margin = new System.Windows.Forms.Padding(4);
            this.cmbKitap.Name = "cmbKitap";
            this.cmbKitap.Size = new System.Drawing.Size(177, 42);
            this.cmbKitap.TabIndex = 78;
            this.cmbKitap.Tag = "";
            this.cmbKitap.Text = "KİTAP";
            this.cmbKitap.SelectedIndexChanged += new System.EventHandler(this.cmbKitap_SelectedIndexChanged);
            this.cmbKitap.TextChanged += new System.EventHandler(this.cmbKitap_TextChanged);
            // 
            // cmbMagazin
            // 
            this.cmbMagazin.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMagazin.ForeColor = System.Drawing.Color.Maroon;
            this.cmbMagazin.FormattingEnabled = true;
            this.cmbMagazin.Items.AddRange(new object[] {
            "Gerçek",
            "Haber",
            "Spor",
            "Bilgisayar",
            "Teknoloji"});
            this.cmbMagazin.Location = new System.Drawing.Point(403, 82);
            this.cmbMagazin.Margin = new System.Windows.Forms.Padding(4);
            this.cmbMagazin.Name = "cmbMagazin";
            this.cmbMagazin.Size = new System.Drawing.Size(177, 42);
            this.cmbMagazin.TabIndex = 79;
            this.cmbMagazin.Text = "DERGİ";
            this.cmbMagazin.SelectedIndexChanged += new System.EventHandler(this.cmbMagazin_SelectedIndexChanged);
            this.cmbMagazin.TextChanged += new System.EventHandler(this.cmbMagazin_TextChanged);
            // 
            // cmbCd
            // 
            this.cmbCd.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCd.ForeColor = System.Drawing.Color.Maroon;
            this.cmbCd.FormattingEnabled = true;
            this.cmbCd.Items.AddRange(new object[] {
            "Romantik",
            "Hard Rock",
            "Ülke"});
            this.cmbCd.Location = new System.Drawing.Point(589, 82);
            this.cmbCd.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCd.Name = "cmbCd";
            this.cmbCd.Size = new System.Drawing.Size(177, 42);
            this.cmbCd.TabIndex = 80;
            this.cmbCd.Text = "MÜZİK CD";
            this.cmbCd.SelectedIndexChanged += new System.EventHandler(this.cmbCd_SelectedIndexChanged);
            this.cmbCd.TextChanged += new System.EventHandler(this.cmbCd_TextChanged);
            // 
            // cmbİndirim
            // 
            this.cmbİndirim.Font = new System.Drawing.Font("Ravie", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbİndirim.ForeColor = System.Drawing.Color.Maroon;
            this.cmbİndirim.FormattingEnabled = true;
            this.cmbİndirim.Location = new System.Drawing.Point(776, 82);
            this.cmbİndirim.Margin = new System.Windows.Forms.Padding(4);
            this.cmbİndirim.Name = "cmbİndirim";
            this.cmbİndirim.Size = new System.Drawing.Size(177, 42);
            this.cmbİndirim.TabIndex = 81;
            this.cmbİndirim.Text = "İNDİRİM";
            this.cmbİndirim.TextChanged += new System.EventHandler(this.cmbİndirim_TextChanged);
            // 
            // pboxUstPanel
            // 
            this.pboxUstPanel.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pboxUstPanel.Location = new System.Drawing.Point(-1, 0);
            this.pboxUstPanel.Margin = new System.Windows.Forms.Padding(4);
            this.pboxUstPanel.Name = "pboxUstPanel";
            this.pboxUstPanel.Size = new System.Drawing.Size(1743, 38);
            this.pboxUstPanel.TabIndex = 82;
            this.pboxUstPanel.TabStop = false;
            // 
            // linklblUyeGirisi
            // 
            this.linklblUyeGirisi.BackColor = System.Drawing.SystemColors.HotTrack;
            this.linklblUyeGirisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.linklblUyeGirisi.LinkColor = System.Drawing.SystemColors.ButtonFace;
            this.linklblUyeGirisi.Location = new System.Drawing.Point(1304, 11);
            this.linklblUyeGirisi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linklblUyeGirisi.Name = "linklblUyeGirisi";
            this.linklblUyeGirisi.Size = new System.Drawing.Size(120, 27);
            this.linklblUyeGirisi.TabIndex = 83;
            this.linklblUyeGirisi.TabStop = true;
            this.linklblUyeGirisi.Text = "ÜYE GİRİŞİ";
            this.linklblUyeGirisi.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linklblUyeGirisi_LinkClicked);
            this.linklblUyeGirisi.MouseClick += new System.Windows.Forms.MouseEventHandler(this.linklblUyeGirisi_MouseClick);
            // 
            // linklblKayitOl
            // 
            this.linklblKayitOl.BackColor = System.Drawing.SystemColors.HotTrack;
            this.linklblKayitOl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.linklblKayitOl.LinkColor = System.Drawing.SystemColors.ButtonFace;
            this.linklblKayitOl.Location = new System.Drawing.Point(1432, 11);
            this.linklblKayitOl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linklblKayitOl.Name = "linklblKayitOl";
            this.linklblKayitOl.Size = new System.Drawing.Size(111, 27);
            this.linklblKayitOl.TabIndex = 84;
            this.linklblKayitOl.TabStop = true;
            this.linklblKayitOl.Text = "KAYIT OL";
            this.linklblKayitOl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linklblKayitOl_LinkClicked);
            this.linklblKayitOl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.linklblKayitOl_MouseClick);
            // 
            // pboxBayrak
            // 
            this.pboxBayrak.Image = ((System.Drawing.Image)(resources.GetObject("pboxBayrak.Image")));
            this.pboxBayrak.Location = new System.Drawing.Point(1669, 0);
            this.pboxBayrak.Margin = new System.Windows.Forms.Padding(4);
            this.pboxBayrak.Name = "pboxBayrak";
            this.pboxBayrak.Size = new System.Drawing.Size(72, 38);
            this.pboxBayrak.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxBayrak.TabIndex = 85;
            this.pboxBayrak.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 18);
            this.label1.TabIndex = 86;
            this.label1.Text = "ÜCRETSİZ İADE";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.lblFiyat);
            this.groupBox1.Controls.Add(this.lblBilgi);
            this.groupBox1.Controls.Add(this.pboxUrun1);
            this.groupBox1.Location = new System.Drawing.Point(15, 314);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(180, 234);
            this.groupBox1.TabIndex = 87;
            this.groupBox1.TabStop = false;
            // 
            // lblFiyat
            // 
            this.lblFiyat.AutoSize = true;
            this.lblFiyat.Location = new System.Drawing.Point(67, 203);
            this.lblFiyat.Name = "lblFiyat";
            this.lblFiyat.Size = new System.Drawing.Size(46, 17);
            this.lblFiyat.TabIndex = 14;
            this.lblFiyat.Text = "FİYAT";
            // 
            // lblBilgi
            // 
            this.lblBilgi.AutoSize = true;
            this.lblBilgi.Location = new System.Drawing.Point(43, 167);
            this.lblBilgi.Name = "lblBilgi";
            this.lblBilgi.Size = new System.Drawing.Size(86, 17);
            this.lblBilgi.TabIndex = 13;
            this.lblBilgi.Text = "ÜRÜN BİLGİ";
            // 
            // pboxUrun1
            // 
            this.pboxUrun1.Image = ((System.Drawing.Image)(resources.GetObject("pboxUrun1.Image")));
            this.pboxUrun1.Location = new System.Drawing.Point(17, 21);
            this.pboxUrun1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pboxUrun1.Name = "pboxUrun1";
            this.pboxUrun1.Size = new System.Drawing.Size(145, 133);
            this.pboxUrun1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxUrun1.TabIndex = 8;
            this.pboxUrun1.TabStop = false;
            this.pboxUrun1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxUrun0_MouseClick);
            // 
            // pboxAra
            // 
            this.pboxAra.Image = ((System.Drawing.Image)(resources.GetObject("pboxAra.Image")));
            this.pboxAra.Location = new System.Drawing.Point(1363, 82);
            this.pboxAra.Margin = new System.Windows.Forms.Padding(4);
            this.pboxAra.Name = "pboxAra";
            this.pboxAra.Size = new System.Drawing.Size(61, 42);
            this.pboxAra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxAra.TabIndex = 88;
            this.pboxAra.TabStop = false;
            this.pboxAra.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxAra_MouseClick);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pboxUrun2);
            this.groupBox2.Location = new System.Drawing.Point(15, 553);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(180, 235);
            this.groupBox2.TabIndex = 88;
            this.groupBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "FİYAT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "ÜRÜN BİLGİ";
            // 
            // pboxUrun2
            // 
            this.pboxUrun2.Image = ((System.Drawing.Image)(resources.GetObject("pboxUrun2.Image")));
            this.pboxUrun2.Location = new System.Drawing.Point(17, 21);
            this.pboxUrun2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pboxUrun2.Name = "pboxUrun2";
            this.pboxUrun2.Size = new System.Drawing.Size(145, 133);
            this.pboxUrun2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxUrun2.TabIndex = 8;
            this.pboxUrun2.TabStop = false;
            this.pboxUrun2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxUrun0_MouseClick);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.pboxUrun3);
            this.groupBox3.Location = new System.Drawing.Point(1547, 314);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(180, 234);
            this.groupBox3.TabIndex = 89;
            this.groupBox3.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "FİYAT";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "ÜRÜN BİLGİ";
            // 
            // pboxUrun3
            // 
            this.pboxUrun3.Image = ((System.Drawing.Image)(resources.GetObject("pboxUrun3.Image")));
            this.pboxUrun3.Location = new System.Drawing.Point(17, 21);
            this.pboxUrun3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pboxUrun3.Name = "pboxUrun3";
            this.pboxUrun3.Size = new System.Drawing.Size(145, 133);
            this.pboxUrun3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxUrun3.TabIndex = 8;
            this.pboxUrun3.TabStop = false;
            this.pboxUrun3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxUrun0_MouseClick);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.pboxUrun4);
            this.groupBox4.Location = new System.Drawing.Point(1547, 553);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(180, 235);
            this.groupBox4.TabIndex = 88;
            this.groupBox4.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(67, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "FİYAT";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "ÜRÜN BİLGİ";
            // 
            // pboxUrun4
            // 
            this.pboxUrun4.Image = ((System.Drawing.Image)(resources.GetObject("pboxUrun4.Image")));
            this.pboxUrun4.Location = new System.Drawing.Point(17, 21);
            this.pboxUrun4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pboxUrun4.Name = "pboxUrun4";
            this.pboxUrun4.Size = new System.Drawing.Size(145, 133);
            this.pboxUrun4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxUrun4.TabIndex = 8;
            this.pboxUrun4.TabStop = false;
            this.pboxUrun4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxUrun0_MouseClick);
            // 
            // pboxCocuk
            // 
            this.pboxCocuk.Image = ((System.Drawing.Image)(resources.GetObject("pboxCocuk.Image")));
            this.pboxCocuk.Location = new System.Drawing.Point(216, 130);
            this.pboxCocuk.Margin = new System.Windows.Forms.Padding(4);
            this.pboxCocuk.Name = "pboxCocuk";
            this.pboxCocuk.Size = new System.Drawing.Size(1307, 265);
            this.pboxCocuk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxCocuk.TabIndex = 90;
            this.pboxCocuk.TabStop = false;
            this.pboxCocuk.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxCocuk_MouseClick);
            // 
            // pboxKullaniciResim
            // 
            this.pboxKullaniciResim.Image = ((System.Drawing.Image)(resources.GetObject("pboxKullaniciResim.Image")));
            this.pboxKullaniciResim.Location = new System.Drawing.Point(1588, 46);
            this.pboxKullaniciResim.Margin = new System.Windows.Forms.Padding(4);
            this.pboxKullaniciResim.Name = "pboxKullaniciResim";
            this.pboxKullaniciResim.Size = new System.Drawing.Size(121, 111);
            this.pboxKullaniciResim.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxKullaniciResim.TabIndex = 91;
            this.pboxKullaniciResim.TabStop = false;
            // 
            // btnKuponlarım
            // 
            this.btnKuponlarım.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnKuponlarım.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKuponlarım.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnKuponlarım.Location = new System.Drawing.Point(1588, 271);
            this.btnKuponlarım.Margin = new System.Windows.Forms.Padding(4);
            this.btnKuponlarım.Name = "btnKuponlarım";
            this.btnKuponlarım.Size = new System.Drawing.Size(121, 28);
            this.btnKuponlarım.TabIndex = 95;
            this.btnKuponlarım.Text = "KUPONLARIM";
            this.btnKuponlarım.UseVisualStyleBackColor = false;
            // 
            // btnKartlarım
            // 
            this.btnKartlarım.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnKartlarım.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKartlarım.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnKartlarım.Location = new System.Drawing.Point(1588, 235);
            this.btnKartlarım.Margin = new System.Windows.Forms.Padding(4);
            this.btnKartlarım.Name = "btnKartlarım";
            this.btnKartlarım.Size = new System.Drawing.Size(121, 28);
            this.btnKartlarım.TabIndex = 94;
            this.btnKartlarım.Text = "KARTLARIM";
            this.btnKartlarım.UseVisualStyleBackColor = false;
            // 
            // btnSiparislerim
            // 
            this.btnSiparislerim.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSiparislerim.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSiparislerim.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSiparislerim.Location = new System.Drawing.Point(1588, 199);
            this.btnSiparislerim.Margin = new System.Windows.Forms.Padding(4);
            this.btnSiparislerim.Name = "btnSiparislerim";
            this.btnSiparislerim.Size = new System.Drawing.Size(121, 28);
            this.btnSiparislerim.TabIndex = 93;
            this.btnSiparislerim.Text = "SİPARİŞLERİM";
            this.btnSiparislerim.UseVisualStyleBackColor = false;
            // 
            // btnProfilim
            // 
            this.btnProfilim.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnProfilim.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnProfilim.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnProfilim.Location = new System.Drawing.Point(1588, 164);
            this.btnProfilim.Margin = new System.Windows.Forms.Padding(4);
            this.btnProfilim.Name = "btnProfilim";
            this.btnProfilim.Size = new System.Drawing.Size(121, 28);
            this.btnProfilim.TabIndex = 92;
            this.btnProfilim.Text = "PROFİLİM";
            this.btnProfilim.UseVisualStyleBackColor = false;
            this.btnProfilim.Click += new System.EventHandler(this.btnProfilim_Click_1);
            // 
            // pboxCizgiRoman
            // 
            this.pboxCizgiRoman.Image = ((System.Drawing.Image)(resources.GetObject("pboxCizgiRoman.Image")));
            this.pboxCizgiRoman.Location = new System.Drawing.Point(907, 402);
            this.pboxCizgiRoman.Margin = new System.Windows.Forms.Padding(4);
            this.pboxCizgiRoman.Name = "pboxCizgiRoman";
            this.pboxCizgiRoman.Size = new System.Drawing.Size(616, 348);
            this.pboxCizgiRoman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxCizgiRoman.TabIndex = 96;
            this.pboxCizgiRoman.TabStop = false;
            this.pboxCizgiRoman.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxCizgiRoman_MouseClick);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.pboxUrun0);
            this.groupBox5.Location = new System.Drawing.Point(15, 82);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(180, 226);
            this.groupBox5.TabIndex = 88;
            this.groupBox5.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(67, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "FİYAT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(43, 167);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "ÜRÜN BİLGİ";
            // 
            // pboxUrun0
            // 
            this.pboxUrun0.Image = ((System.Drawing.Image)(resources.GetObject("pboxUrun0.Image")));
            this.pboxUrun0.Location = new System.Drawing.Point(17, 21);
            this.pboxUrun0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pboxUrun0.Name = "pboxUrun0";
            this.pboxUrun0.Size = new System.Drawing.Size(145, 133);
            this.pboxUrun0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxUrun0.TabIndex = 8;
            this.pboxUrun0.TabStop = false;
            this.pboxUrun0.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxUrun0_MouseClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pictureBox1.Location = new System.Drawing.Point(216, 756);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1307, 31);
            this.pictureBox1.TabIndex = 97;
            this.pictureBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(248, 759);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 27);
            this.label10.TabIndex = 98;
            this.label10.Text = "İLETİŞİM";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label11.Location = new System.Drawing.Point(376, 759);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 18);
            this.label11.TabIndex = 99;
            this.label11.Text = "+905435113035";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label12.Location = new System.Drawing.Point(563, 759);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(210, 18);
            this.label12.TabIndex = 100;
            this.label12.Text = "sametsalgin10@gmail.com";
            // 
            // linklblCikisYap
            // 
            this.linklblCikisYap.BackColor = System.Drawing.SystemColors.HotTrack;
            this.linklblCikisYap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.linklblCikisYap.LinkColor = System.Drawing.SystemColors.ButtonFace;
            this.linklblCikisYap.Location = new System.Drawing.Point(1534, 11);
            this.linklblCikisYap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linklblCikisYap.Name = "linklblCikisYap";
            this.linklblCikisYap.Size = new System.Drawing.Size(111, 27);
            this.linklblCikisYap.TabIndex = 101;
            this.linklblCikisYap.TabStop = true;
            this.linklblCikisYap.Text = "ÇIKIŞ YAP";
            this.linklblCikisYap.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linklblCikisYap_LinkClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1741, 826);
            this.Controls.Add(this.linklblCikisYap);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.pboxCizgiRoman);
            this.Controls.Add(this.btnKuponlarım);
            this.Controls.Add(this.btnKartlarım);
            this.Controls.Add(this.btnSiparislerim);
            this.Controls.Add(this.btnProfilim);
            this.Controls.Add(this.pboxKullaniciResim);
            this.Controls.Add(this.pboxCocuk);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pboxAra);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pboxBayrak);
            this.Controls.Add(this.linklblKayitOl);
            this.Controls.Add(this.linklblUyeGirisi);
            this.Controls.Add(this.pboxUstPanel);
            this.Controls.Add(this.cmbİndirim);
            this.Controls.Add(this.cmbCd);
            this.Controls.Add(this.cmbMagazin);
            this.Controls.Add(this.cmbKitap);
            this.Controls.Add(this.pboxKampanya);
            this.Controls.Add(this.pboxSepet);
            this.Controls.Add(this.txtArama);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "ANA EKRAN";
            ((System.ComponentModel.ISupportInitialize)(this.pboxSepet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxKampanya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUstPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBayrak)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxAra)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun3)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCocuk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxKullaniciResim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCizgiRoman)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxUrun0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtArama;
        private System.Windows.Forms.PictureBox pboxSepet;
        private System.Windows.Forms.PictureBox pboxKampanya;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cmbKitap;
        private System.Windows.Forms.ComboBox cmbMagazin;
        private System.Windows.Forms.ComboBox cmbCd;
        private System.Windows.Forms.ComboBox cmbİndirim;
        private System.Windows.Forms.PictureBox pboxUstPanel;
        private System.Windows.Forms.PictureBox pboxBayrak;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblFiyat;
        private System.Windows.Forms.Label lblBilgi;
        private System.Windows.Forms.PictureBox pboxUrun1;
        private System.Windows.Forms.PictureBox pboxAra;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pboxUrun2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pboxUrun3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pboxUrun4;
        private System.Windows.Forms.PictureBox pboxCocuk;
        private System.Windows.Forms.PictureBox pboxKullaniciResim;
        private System.Windows.Forms.Button btnKuponlarım;
        private System.Windows.Forms.Button btnKartlarım;
        private System.Windows.Forms.Button btnSiparislerim;
        private System.Windows.Forms.Button btnProfilim;
        private System.Windows.Forms.PictureBox pboxCizgiRoman;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pboxUrun0;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.LinkLabel linklblCikisYap;
        public System.Windows.Forms.LinkLabel linklblUyeGirisi;
        public System.Windows.Forms.LinkLabel linklblKayitOl;
    }
}

