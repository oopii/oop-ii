﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace OOP2_PROJE
{
    class SQLmusicCdCek:SQL_Veri_Cek
    {
        private SqlConnection bagla = new SqlConnection("Data Source=.;Initial Catalog=BookStore;Integrated Security=true");
        SqlCommand komut;
        SqlDataReader veriOku;

        public SqlDataReader VeriOku { get => veriOku; set => veriOku = value; }
        public SqlCommand Komut { get => komut; set => komut = value; }
        public SqlConnection Bagla { get => bagla; set => bagla = value; }

        MusicCD MusicCDNesnesi = new MusicCD();
        FormAdmin formAdmin_obj = new FormAdmin();
        FormUrünler formUrünler_obj = new FormUrünler();


        public void muzikCDleriSQLdenCek()
        {
            bagla.Open();
            komut = new SqlCommand("SELECT musicCDID,musicCDName,musicCDPrice,musicCDSinger FROM TableMusicCD", Bagla);
            VeriOku = komut.ExecuteReader();
            List<MusicCD> muzikCDListesi = new List<MusicCD>();
            while (VeriOku.Read())
            {
                MusicCD nesneTut = new MusicCD();
                MusicCDNesnesi.ID = VeriOku["musicCDID"].ToString();
                MusicCDNesnesi.name = VeriOku["musicCDName"].ToString();
                MusicCDNesnesi.price = double.Parse(VeriOku["musicCDPrice"].ToString());
                MusicCDNesnesi.Singer = VeriOku["musicCDSinger"].ToString();

                nesneTut.id = MusicCDNesnesi.id;
                nesneTut.name = MusicCDNesnesi.name;
                nesneTut.price = MusicCDNesnesi.price;
                nesneTut.Singer = MusicCDNesnesi.Singer;

                muzikCDListesi.Add(nesneTut);
            }
            formUrünler_obj.Show();
            bagla.Close();
            formUrünler_obj.muzikCDleriGoster(muzikCDListesi);
        }
    }
}
