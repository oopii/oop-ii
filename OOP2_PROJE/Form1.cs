﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OOP2_PROJE
{
    
    public partial class Form1 : Form
    {
        int girisResimIndex = 0;

        Book bookNesnesi = new Book();
        Magazine magazineNesnesi = new Magazine();
        MusicCD musicCDnesnesi = new MusicCD();
        Customer customerNesnesi;

        public Form1()
        {
            InitializeComponent();
        }
       
        
        private void girisKampanyaGoster()
        {
            girisResimIndex = girisResimIndex - 1;
            if (girisResimIndex <= 0)
            {
                girisResimIndex = 2;
            }

            pboxKampanya.ImageLocation = "Resim" + girisResimIndex + ".jpg";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            girisKampanyaGoster();
        }

        private void btnProfilim_Click(object sender, EventArgs e)
        {
            //profil sayfası bilgiler

        }

        private void btnSiparislerim_Click(object sender, EventArgs e)
        {
            pboxUstPanel.ImageLocation = "Resim" + 1 + ".jpg";
            //suanki ve gecmıs sıparısler
        }

        private void btnKartlarım_Click(object sender, EventArgs e)
        {
            //kredı kartı bılgılerı
        }

        private void btnKuponlarım_Click(object sender, EventArgs e)
        {
            //tanımlı kuponlarım
        }

        private void pboxCocuk_MouseClick(object sender, MouseEventArgs e)
        {
            // tüm ürünler veya cocuk ürünleri

        }

        private void pboxKampanya_MouseClick(object sender, MouseEventArgs e)
        {
            // tüm ürünler
        }

        private void pboxCizgiRoman_MouseClick(object sender, MouseEventArgs e)
        {
            //cizgi romanlar

        }

        private void pboxUrun0_MouseClick(object sender, MouseEventArgs e)
        {
            // saddece o ürünün sayfası
            //objenin bilgileri
            //her yandakı urunler ıcın tek fonk oda bu
        }

        private void pboxAra_MouseClick(object sender, MouseEventArgs e)
        {
            // arama yapar mouse click ile

        }

        private void txtArama_Enter(object sender, EventArgs e)
        {
            //arama yapar enter ile

        }

        private void linklblUyeGirisi_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormUyeGirisi formUyeGirisi = new FormUyeGirisi();
            formUyeGirisi.Show();
        }

        private void linklblKayitOl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormKayitOl formKayitOl_obj = new FormKayitOl();
            formKayitOl_obj.Show();
        }

        private void linklblUyeGirisi_MouseClick(object sender, MouseEventArgs e)
        {
            //üye giriş
            
            //this.Hide();
        }

        private void linklblKayitOl_MouseClick(object sender, MouseEventArgs e)
        {
            //kayıt ol
        }

        private void pboxSepet_MouseClick(object sender, MouseEventArgs e)
        {
            //sepete gider
            MessageBox.Show("pboxSepet_MouseClick");
        }

        private void btnProfilim_Click_1(object sender, EventArgs e)
        {
            FormProfilim formProfilim_obj = new FormProfilim();
            formProfilim_obj.Show();
            formProfilim_obj.ProfilBilgiGir(customerNesnesi);
        }

        private void cmbMagazin_SelectedIndexChanged(object sender, EventArgs e)
        {
            //secili tür için form açılacak
            SQLdergiCek sqlDergiCek = new SQLdergiCek();
            sqlDergiCek.dergileriSQLdenCek(); 
        }

        private void cmbKitap_SelectedIndexChanged(object sender, EventArgs e)
        {
            //secili tür için form açılacak
            SQLkitapCek kitapCek = new SQLkitapCek();
            kitapCek.kitaplariSQLdenCek();
            
        }

        private void linklblCikisYap_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linklblUyeGirisi.Visible = true;
            linklblKayitOl.Visible = true;
            customerNesnesi = null;

        }

        private void cmbKitap_TextChanged(object sender, EventArgs e)
        {
            cmbKitap.Text = "KİTAP";
        }

        private void cmbMagazin_TextChanged(object sender, EventArgs e)
        {
            cmbMagazin.Text = "DERGİ";
        }

        private void cmbCd_TextChanged(object sender, EventArgs e)
        {
            cmbCd.Text = "MÜZİK CD";
        }

        private void cmbİndirim_TextChanged(object sender, EventArgs e)
        {
            cmbİndirim.Text = "İNDİRİM";
        }

        private void cmbCd_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQLmusicCdCek muzikCdCek = new SQLmusicCdCek();
            muzikCdCek.muzikCDleriSQLdenCek();
        }
    }
}
