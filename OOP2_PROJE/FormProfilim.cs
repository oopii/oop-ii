﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_PROJE
{
    public partial class FormProfilim : Form
    {
        public FormProfilim()
        {
            InitializeComponent();
        }

        private void FormProfilim_Load(object sender, EventArgs e)
        {

        }
        public void ProfilBilgiGir(User userNesnesi)
        {
            if(userNesnesi != null)
            {
                lblProfilAdi.Text = userNesnesi.name;
                lblProfilAdres.Text = userNesnesi.address;
                lblProfilEmail.Text = userNesnesi.email;
                lblProfilKullaniciAdi.Text = userNesnesi.userName;
            }
        }
        
    }
}
