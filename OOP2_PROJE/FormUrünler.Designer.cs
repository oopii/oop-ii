﻿namespace OOP2_PROJE
{
    partial class FormUrünler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbl5Fiyat = new System.Windows.Forms.Label();
            this.lbl5Sayfa = new System.Windows.Forms.Label();
            this.lbl5Yayinci = new System.Windows.Forms.Label();
            this.lbl5Yazar = new System.Windows.Forms.Label();
            this.lbl5Adi = new System.Windows.Forms.Label();
            this.pbx5 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbl4Fiyat = new System.Windows.Forms.Label();
            this.lbl4Sayfa = new System.Windows.Forms.Label();
            this.lbl4Yayinci = new System.Windows.Forms.Label();
            this.lbl4Yazar = new System.Windows.Forms.Label();
            this.lbl4Adi = new System.Windows.Forms.Label();
            this.pbx4 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbl3Fiyat = new System.Windows.Forms.Label();
            this.lbl3Sayfa = new System.Windows.Forms.Label();
            this.lbl3Yayinci = new System.Windows.Forms.Label();
            this.lbl3Yazar = new System.Windows.Forms.Label();
            this.lbl3Adi = new System.Windows.Forms.Label();
            this.pbx3 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl2Fiyat = new System.Windows.Forms.Label();
            this.lbl2Sayfa = new System.Windows.Forms.Label();
            this.lbl2Yayinci = new System.Windows.Forms.Label();
            this.lbl2Yazar = new System.Windows.Forms.Label();
            this.lbl2Adi = new System.Windows.Forms.Label();
            this.pbx2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl1Fiyat = new System.Windows.Forms.Label();
            this.lbl1Sayfa = new System.Windows.Forms.Label();
            this.lbl1Yayinci = new System.Windows.Forms.Label();
            this.lbl1Yazar = new System.Windows.Forms.Label();
            this.lbl1Adi = new System.Windows.Forms.Label();
            this.pbx1 = new System.Windows.Forms.PictureBox();
            this.btnIleri = new System.Windows.Forms.Button();
            this.btnGeri = new System.Windows.Forms.Button();
            this.btnAra = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1deneme = new System.Windows.Forms.Label();
            this.btnIleriGoster = new System.Windows.Forms.Button();
            this.btnGeriGoster = new System.Windows.Forms.Button();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx5)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx4)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx3)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbl5Fiyat);
            this.groupBox5.Controls.Add(this.lbl5Sayfa);
            this.groupBox5.Controls.Add(this.lbl5Yayinci);
            this.groupBox5.Controls.Add(this.lbl5Yazar);
            this.groupBox5.Controls.Add(this.lbl5Adi);
            this.groupBox5.Controls.Add(this.pbx5);
            this.groupBox5.Location = new System.Drawing.Point(1196, 115);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(277, 543);
            this.groupBox5.TabIndex = 75;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "groupBox5";
            // 
            // lbl5Fiyat
            // 
            this.lbl5Fiyat.AutoSize = true;
            this.lbl5Fiyat.Location = new System.Drawing.Point(16, 524);
            this.lbl5Fiyat.Name = "lbl5Fiyat";
            this.lbl5Fiyat.Size = new System.Drawing.Size(50, 17);
            this.lbl5Fiyat.TabIndex = 23;
            this.lbl5Fiyat.Text = "Fiyat : ";
            // 
            // lbl5Sayfa
            // 
            this.lbl5Sayfa.AutoSize = true;
            this.lbl5Sayfa.Location = new System.Drawing.Point(16, 491);
            this.lbl5Sayfa.Name = "lbl5Sayfa";
            this.lbl5Sayfa.Size = new System.Drawing.Size(56, 17);
            this.lbl5Sayfa.TabIndex = 20;
            this.lbl5Sayfa.Text = "Sayfa : ";
            // 
            // lbl5Yayinci
            // 
            this.lbl5Yayinci.AutoSize = true;
            this.lbl5Yayinci.Location = new System.Drawing.Point(16, 458);
            this.lbl5Yayinci.Name = "lbl5Yayinci";
            this.lbl5Yayinci.Size = new System.Drawing.Size(55, 17);
            this.lbl5Yayinci.TabIndex = 19;
            this.lbl5Yayinci.Text = "Yayın : ";
            // 
            // lbl5Yazar
            // 
            this.lbl5Yazar.AutoSize = true;
            this.lbl5Yazar.Location = new System.Drawing.Point(16, 420);
            this.lbl5Yazar.Name = "lbl5Yazar";
            this.lbl5Yazar.Size = new System.Drawing.Size(60, 17);
            this.lbl5Yazar.TabIndex = 18;
            this.lbl5Yazar.Text = "Yazarı : ";
            // 
            // lbl5Adi
            // 
            this.lbl5Adi.AutoSize = true;
            this.lbl5Adi.Location = new System.Drawing.Point(16, 384);
            this.lbl5Adi.Name = "lbl5Adi";
            this.lbl5Adi.Size = new System.Drawing.Size(76, 17);
            this.lbl5Adi.TabIndex = 17;
            this.lbl5Adi.Text = "Kitap Adı : ";
            // 
            // pbx5
            // 
            this.pbx5.Location = new System.Drawing.Point(19, 28);
            this.pbx5.Name = "pbx5";
            this.pbx5.Size = new System.Drawing.Size(231, 335);
            this.pbx5.TabIndex = 12;
            this.pbx5.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbl4Fiyat);
            this.groupBox4.Controls.Add(this.lbl4Sayfa);
            this.groupBox4.Controls.Add(this.lbl4Yayinci);
            this.groupBox4.Controls.Add(this.lbl4Yazar);
            this.groupBox4.Controls.Add(this.lbl4Adi);
            this.groupBox4.Controls.Add(this.pbx4);
            this.groupBox4.Location = new System.Drawing.Point(907, 115);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(273, 543);
            this.groupBox4.TabIndex = 74;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // lbl4Fiyat
            // 
            this.lbl4Fiyat.AutoSize = true;
            this.lbl4Fiyat.Location = new System.Drawing.Point(12, 524);
            this.lbl4Fiyat.Name = "lbl4Fiyat";
            this.lbl4Fiyat.Size = new System.Drawing.Size(50, 17);
            this.lbl4Fiyat.TabIndex = 22;
            this.lbl4Fiyat.Text = "Fiyat : ";
            // 
            // lbl4Sayfa
            // 
            this.lbl4Sayfa.AutoSize = true;
            this.lbl4Sayfa.Location = new System.Drawing.Point(12, 491);
            this.lbl4Sayfa.Name = "lbl4Sayfa";
            this.lbl4Sayfa.Size = new System.Drawing.Size(56, 17);
            this.lbl4Sayfa.TabIndex = 20;
            this.lbl4Sayfa.Text = "Sayfa : ";
            // 
            // lbl4Yayinci
            // 
            this.lbl4Yayinci.AutoSize = true;
            this.lbl4Yayinci.Location = new System.Drawing.Point(12, 458);
            this.lbl4Yayinci.Name = "lbl4Yayinci";
            this.lbl4Yayinci.Size = new System.Drawing.Size(55, 17);
            this.lbl4Yayinci.TabIndex = 19;
            this.lbl4Yayinci.Text = "Yayın : ";
            // 
            // lbl4Yazar
            // 
            this.lbl4Yazar.AutoSize = true;
            this.lbl4Yazar.Location = new System.Drawing.Point(12, 420);
            this.lbl4Yazar.Name = "lbl4Yazar";
            this.lbl4Yazar.Size = new System.Drawing.Size(60, 17);
            this.lbl4Yazar.TabIndex = 18;
            this.lbl4Yazar.Text = "Yazarı : ";
            // 
            // lbl4Adi
            // 
            this.lbl4Adi.AutoSize = true;
            this.lbl4Adi.Location = new System.Drawing.Point(12, 384);
            this.lbl4Adi.Name = "lbl4Adi";
            this.lbl4Adi.Size = new System.Drawing.Size(76, 17);
            this.lbl4Adi.TabIndex = 17;
            this.lbl4Adi.Text = "Kitap Adı : ";
            // 
            // pbx4
            // 
            this.pbx4.Location = new System.Drawing.Point(15, 28);
            this.pbx4.Name = "pbx4";
            this.pbx4.Size = new System.Drawing.Size(231, 335);
            this.pbx4.TabIndex = 11;
            this.pbx4.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbl3Fiyat);
            this.groupBox3.Controls.Add(this.lbl3Sayfa);
            this.groupBox3.Controls.Add(this.lbl3Yayinci);
            this.groupBox3.Controls.Add(this.lbl3Yazar);
            this.groupBox3.Controls.Add(this.lbl3Adi);
            this.groupBox3.Controls.Add(this.pbx3);
            this.groupBox3.Location = new System.Drawing.Point(617, 115);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(270, 543);
            this.groupBox3.TabIndex = 73;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // lbl3Fiyat
            // 
            this.lbl3Fiyat.AutoSize = true;
            this.lbl3Fiyat.Location = new System.Drawing.Point(15, 524);
            this.lbl3Fiyat.Name = "lbl3Fiyat";
            this.lbl3Fiyat.Size = new System.Drawing.Size(50, 17);
            this.lbl3Fiyat.TabIndex = 21;
            this.lbl3Fiyat.Text = "Fiyat : ";
            // 
            // lbl3Sayfa
            // 
            this.lbl3Sayfa.AutoSize = true;
            this.lbl3Sayfa.Location = new System.Drawing.Point(15, 491);
            this.lbl3Sayfa.Name = "lbl3Sayfa";
            this.lbl3Sayfa.Size = new System.Drawing.Size(56, 17);
            this.lbl3Sayfa.TabIndex = 20;
            this.lbl3Sayfa.Text = "Sayfa : ";
            // 
            // lbl3Yayinci
            // 
            this.lbl3Yayinci.AutoSize = true;
            this.lbl3Yayinci.Location = new System.Drawing.Point(15, 458);
            this.lbl3Yayinci.Name = "lbl3Yayinci";
            this.lbl3Yayinci.Size = new System.Drawing.Size(55, 17);
            this.lbl3Yayinci.TabIndex = 19;
            this.lbl3Yayinci.Text = "Yayın : ";
            // 
            // lbl3Yazar
            // 
            this.lbl3Yazar.AutoSize = true;
            this.lbl3Yazar.Location = new System.Drawing.Point(15, 420);
            this.lbl3Yazar.Name = "lbl3Yazar";
            this.lbl3Yazar.Size = new System.Drawing.Size(60, 17);
            this.lbl3Yazar.TabIndex = 18;
            this.lbl3Yazar.Text = "Yazarı : ";
            // 
            // lbl3Adi
            // 
            this.lbl3Adi.AutoSize = true;
            this.lbl3Adi.Location = new System.Drawing.Point(15, 384);
            this.lbl3Adi.Name = "lbl3Adi";
            this.lbl3Adi.Size = new System.Drawing.Size(76, 17);
            this.lbl3Adi.TabIndex = 17;
            this.lbl3Adi.Text = "Kitap Adı : ";
            // 
            // pbx3
            // 
            this.pbx3.Location = new System.Drawing.Point(18, 25);
            this.pbx3.Name = "pbx3";
            this.pbx3.Size = new System.Drawing.Size(231, 335);
            this.pbx3.TabIndex = 10;
            this.pbx3.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl2Fiyat);
            this.groupBox2.Controls.Add(this.lbl2Sayfa);
            this.groupBox2.Controls.Add(this.lbl2Yayinci);
            this.groupBox2.Controls.Add(this.lbl2Yazar);
            this.groupBox2.Controls.Add(this.lbl2Adi);
            this.groupBox2.Controls.Add(this.pbx2);
            this.groupBox2.Location = new System.Drawing.Point(326, 115);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(277, 543);
            this.groupBox2.TabIndex = 72;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // lbl2Fiyat
            // 
            this.lbl2Fiyat.AutoSize = true;
            this.lbl2Fiyat.Location = new System.Drawing.Point(19, 524);
            this.lbl2Fiyat.Name = "lbl2Fiyat";
            this.lbl2Fiyat.Size = new System.Drawing.Size(50, 17);
            this.lbl2Fiyat.TabIndex = 18;
            this.lbl2Fiyat.Text = "Fiyat : ";
            // 
            // lbl2Sayfa
            // 
            this.lbl2Sayfa.AutoSize = true;
            this.lbl2Sayfa.Location = new System.Drawing.Point(19, 491);
            this.lbl2Sayfa.Name = "lbl2Sayfa";
            this.lbl2Sayfa.Size = new System.Drawing.Size(56, 17);
            this.lbl2Sayfa.TabIndex = 20;
            this.lbl2Sayfa.Text = "Sayfa : ";
            // 
            // lbl2Yayinci
            // 
            this.lbl2Yayinci.AutoSize = true;
            this.lbl2Yayinci.Location = new System.Drawing.Point(19, 458);
            this.lbl2Yayinci.Name = "lbl2Yayinci";
            this.lbl2Yayinci.Size = new System.Drawing.Size(55, 17);
            this.lbl2Yayinci.TabIndex = 19;
            this.lbl2Yayinci.Text = "Yayın : ";
            // 
            // lbl2Yazar
            // 
            this.lbl2Yazar.AutoSize = true;
            this.lbl2Yazar.Location = new System.Drawing.Point(19, 420);
            this.lbl2Yazar.Name = "lbl2Yazar";
            this.lbl2Yazar.Size = new System.Drawing.Size(60, 17);
            this.lbl2Yazar.TabIndex = 18;
            this.lbl2Yazar.Text = "Yazarı : ";
            // 
            // lbl2Adi
            // 
            this.lbl2Adi.AutoSize = true;
            this.lbl2Adi.Location = new System.Drawing.Point(19, 384);
            this.lbl2Adi.Name = "lbl2Adi";
            this.lbl2Adi.Size = new System.Drawing.Size(76, 17);
            this.lbl2Adi.TabIndex = 17;
            this.lbl2Adi.Text = "Kitap Adı : ";
            // 
            // pbx2
            // 
            this.pbx2.Location = new System.Drawing.Point(22, 25);
            this.pbx2.Name = "pbx2";
            this.pbx2.Size = new System.Drawing.Size(231, 335);
            this.pbx2.TabIndex = 9;
            this.pbx2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl1Fiyat);
            this.groupBox1.Controls.Add(this.lbl1Sayfa);
            this.groupBox1.Controls.Add(this.lbl1Yayinci);
            this.groupBox1.Controls.Add(this.lbl1Yazar);
            this.groupBox1.Controls.Add(this.lbl1Adi);
            this.groupBox1.Controls.Add(this.pbx1);
            this.groupBox1.Location = new System.Drawing.Point(34, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 544);
            this.groupBox1.TabIndex = 71;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // lbl1Fiyat
            // 
            this.lbl1Fiyat.AutoSize = true;
            this.lbl1Fiyat.Location = new System.Drawing.Point(14, 524);
            this.lbl1Fiyat.Name = "lbl1Fiyat";
            this.lbl1Fiyat.Size = new System.Drawing.Size(50, 17);
            this.lbl1Fiyat.TabIndex = 17;
            this.lbl1Fiyat.Text = "Fiyat : ";
            // 
            // lbl1Sayfa
            // 
            this.lbl1Sayfa.AutoSize = true;
            this.lbl1Sayfa.Location = new System.Drawing.Point(14, 491);
            this.lbl1Sayfa.Name = "lbl1Sayfa";
            this.lbl1Sayfa.Size = new System.Drawing.Size(56, 17);
            this.lbl1Sayfa.TabIndex = 16;
            this.lbl1Sayfa.Text = "Sayfa : ";
            // 
            // lbl1Yayinci
            // 
            this.lbl1Yayinci.AutoSize = true;
            this.lbl1Yayinci.Location = new System.Drawing.Point(14, 458);
            this.lbl1Yayinci.Name = "lbl1Yayinci";
            this.lbl1Yayinci.Size = new System.Drawing.Size(55, 17);
            this.lbl1Yayinci.TabIndex = 15;
            this.lbl1Yayinci.Text = "Yayın : ";
            // 
            // lbl1Yazar
            // 
            this.lbl1Yazar.AutoSize = true;
            this.lbl1Yazar.Location = new System.Drawing.Point(14, 420);
            this.lbl1Yazar.Name = "lbl1Yazar";
            this.lbl1Yazar.Size = new System.Drawing.Size(60, 17);
            this.lbl1Yazar.TabIndex = 14;
            this.lbl1Yazar.Text = "Yazarı : ";
            // 
            // lbl1Adi
            // 
            this.lbl1Adi.AutoSize = true;
            this.lbl1Adi.Location = new System.Drawing.Point(14, 384);
            this.lbl1Adi.Name = "lbl1Adi";
            this.lbl1Adi.Size = new System.Drawing.Size(76, 17);
            this.lbl1Adi.TabIndex = 13;
            this.lbl1Adi.Text = "Kitap Adı : ";
            // 
            // pbx1
            // 
            this.pbx1.Location = new System.Drawing.Point(17, 28);
            this.pbx1.Name = "pbx1";
            this.pbx1.Size = new System.Drawing.Size(231, 335);
            this.pbx1.TabIndex = 8;
            this.pbx1.TabStop = false;
            // 
            // btnIleri
            // 
            this.btnIleri.Location = new System.Drawing.Point(92, 19);
            this.btnIleri.Name = "btnIleri";
            this.btnIleri.Size = new System.Drawing.Size(52, 34);
            this.btnIleri.TabIndex = 70;
            this.btnIleri.Text = ">";
            this.btnIleri.UseVisualStyleBackColor = true;
            // 
            // btnGeri
            // 
            this.btnGeri.Location = new System.Drawing.Point(34, 19);
            this.btnGeri.Name = "btnGeri";
            this.btnGeri.Size = new System.Drawing.Size(52, 34);
            this.btnGeri.TabIndex = 69;
            this.btnGeri.Text = "<";
            this.btnGeri.UseVisualStyleBackColor = true;
            // 
            // btnAra
            // 
            this.btnAra.Location = new System.Drawing.Point(789, 19);
            this.btnAra.Name = "btnAra";
            this.btnAra.Size = new System.Drawing.Size(90, 34);
            this.btnAra.TabIndex = 68;
            this.btnAra.Text = "ARA";
            this.btnAra.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBox1.Location = new System.Drawing.Point(163, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(613, 34);
            this.textBox1.TabIndex = 67;
            this.textBox1.Text = "Aradığınız Kitap Adını Girin";
            // 
            // label1deneme
            // 
            this.label1deneme.AutoSize = true;
            this.label1deneme.Location = new System.Drawing.Point(31, 69);
            this.label1deneme.Name = "label1deneme";
            this.label1deneme.Size = new System.Drawing.Size(56, 17);
            this.label1deneme.TabIndex = 18;
            this.label1deneme.Text = "Sayfa : ";
            // 
            // btnIleriGoster
            // 
            this.btnIleriGoster.Location = new System.Drawing.Point(1129, 30);
            this.btnIleriGoster.Name = "btnIleriGoster";
            this.btnIleriGoster.Size = new System.Drawing.Size(67, 27);
            this.btnIleriGoster.TabIndex = 76;
            this.btnIleriGoster.Text = "İLERİ";
            this.btnIleriGoster.UseVisualStyleBackColor = true;
            this.btnIleriGoster.Click += new System.EventHandler(this.btnIleriGoster_Click);
            // 
            // btnGeriGoster
            // 
            this.btnGeriGoster.Location = new System.Drawing.Point(1056, 30);
            this.btnGeriGoster.Name = "btnGeriGoster";
            this.btnGeriGoster.Size = new System.Drawing.Size(67, 27);
            this.btnGeriGoster.TabIndex = 77;
            this.btnGeriGoster.Text = "GERİ";
            this.btnGeriGoster.UseVisualStyleBackColor = true;
            this.btnGeriGoster.Click += new System.EventHandler(this.btnGeriGoster_Click);
            // 
            // FormUrünler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1540, 690);
            this.Controls.Add(this.btnGeriGoster);
            this.Controls.Add(this.btnIleriGoster);
            this.Controls.Add(this.label1deneme);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnIleri);
            this.Controls.Add(this.btnGeri);
            this.Controls.Add(this.btnAra);
            this.Controls.Add(this.textBox1);
            this.Name = "FormUrünler";
            this.Text = "FormKitap";
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx5)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx4)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx3)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox pbx5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pbx4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pbx3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pbx2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbx1;
        private System.Windows.Forms.Button btnIleri;
        private System.Windows.Forms.Button btnGeri;
        private System.Windows.Forms.Button btnAra;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1deneme;
        private System.Windows.Forms.Label lbl4Yazar;
        private System.Windows.Forms.Label lbl4Adi;
        private System.Windows.Forms.Label lbl3Fiyat;
        private System.Windows.Forms.Label lbl3Sayfa;
        private System.Windows.Forms.Label lbl3Yayinci;
        private System.Windows.Forms.Label lbl3Yazar;
        private System.Windows.Forms.Label lbl3Adi;
        private System.Windows.Forms.Label lbl2Fiyat;
        private System.Windows.Forms.Label lbl2Sayfa;
        private System.Windows.Forms.Label lbl2Yayinci;
        private System.Windows.Forms.Label lbl2Yazar;
        private System.Windows.Forms.Label lbl2Adi;
        private System.Windows.Forms.Label lbl1Fiyat;
        private System.Windows.Forms.Label lbl1Sayfa;
        private System.Windows.Forms.Label lbl1Yayinci;
        private System.Windows.Forms.Label lbl1Yazar;
        private System.Windows.Forms.Label lbl1Adi;
        private System.Windows.Forms.Label lbl5Fiyat;
        private System.Windows.Forms.Label lbl5Sayfa;
        private System.Windows.Forms.Label lbl5Yayinci;
        private System.Windows.Forms.Label lbl5Yazar;
        private System.Windows.Forms.Label lbl5Adi;
        private System.Windows.Forms.Label lbl4Fiyat;
        private System.Windows.Forms.Label lbl4Sayfa;
        private System.Windows.Forms.Label lbl4Yayinci;
        private System.Windows.Forms.Button btnIleriGoster;
        private System.Windows.Forms.Button btnGeriGoster;
    }
}