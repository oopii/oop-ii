﻿namespace OOP2_PROJE
{
    partial class FormProfilim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblProfilAdi = new System.Windows.Forms.Label();
            this.lblProfilAdres = new System.Windows.Forms.Label();
            this.lblProfilEmail = new System.Windows.Forms.Label();
            this.lblProfilKullaniciAdi = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(139, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 136);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblProfilAdi
            // 
            this.lblProfilAdi.AutoSize = true;
            this.lblProfilAdi.Location = new System.Drawing.Point(136, 192);
            this.lblProfilAdi.Name = "lblProfilAdi";
            this.lblProfilAdi.Size = new System.Drawing.Size(28, 17);
            this.lblProfilAdi.TabIndex = 1;
            this.lblProfilAdi.Text = "Adı";
            // 
            // lblProfilAdres
            // 
            this.lblProfilAdres.AutoSize = true;
            this.lblProfilAdres.Location = new System.Drawing.Point(136, 219);
            this.lblProfilAdres.Name = "lblProfilAdres";
            this.lblProfilAdres.Size = new System.Drawing.Size(45, 17);
            this.lblProfilAdres.TabIndex = 2;
            this.lblProfilAdres.Text = "Adres";
            // 
            // lblProfilEmail
            // 
            this.lblProfilEmail.AutoSize = true;
            this.lblProfilEmail.Location = new System.Drawing.Point(136, 246);
            this.lblProfilEmail.Name = "lblProfilEmail";
            this.lblProfilEmail.Size = new System.Drawing.Size(42, 17);
            this.lblProfilEmail.TabIndex = 3;
            this.lblProfilEmail.Text = "Email";
            // 
            // lblProfilKullaniciAdi
            // 
            this.lblProfilKullaniciAdi.AutoSize = true;
            this.lblProfilKullaniciAdi.Location = new System.Drawing.Point(136, 275);
            this.lblProfilKullaniciAdi.Name = "lblProfilKullaniciAdi";
            this.lblProfilKullaniciAdi.Size = new System.Drawing.Size(84, 17);
            this.lblProfilKullaniciAdi.TabIndex = 4;
            this.lblProfilKullaniciAdi.Text = "Kullanıcı Adı";
            // 
            // FormProfilim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 430);
            this.Controls.Add(this.lblProfilKullaniciAdi);
            this.Controls.Add(this.lblProfilEmail);
            this.Controls.Add(this.lblProfilAdres);
            this.Controls.Add(this.lblProfilAdi);
            this.Controls.Add(this.pictureBox1);
            this.Name = "FormProfilim";
            this.Text = "FormProfilim";
            this.Load += new System.EventHandler(this.FormProfilim_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblProfilAdi;
        private System.Windows.Forms.Label lblProfilAdres;
        private System.Windows.Forms.Label lblProfilEmail;
        private System.Windows.Forms.Label lblProfilKullaniciAdi;
    }
}