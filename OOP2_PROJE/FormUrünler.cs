﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;

namespace OOP2_PROJE
{
    public partial class FormUrünler : Form
    {
        List<Book> kitapListesi = new List<Book>();
        List<Magazine> dergiListesi = new List<Magazine>();
        List<MusicCD> muzikCdListesi = new List<MusicCD>();
        static int tiklanmaDurumu = 0;
        static int kitapSayac = 0;
        static int dergiSayac = 0;
        static int muzikCdSayac = 0;
        static int kitapGeriSayac= kitapSayac;
        static int dergiGeriSayac=dergiSayac;
        static int muzikCdGeriSayac=muzikCdSayac;

        public FormUrünler()
        {
            muzikCdSayac = 0;
            kitapSayac = 0;
            dergiSayac = 0;
            InitializeComponent();
        }
        internal void kitaplariGoster(List<Book> bookListesi)
        {
            kitapGeriSayac = kitapSayac;
            tiklanmaDurumu = 1;// kitap göstermek için tıklandığında 1 olacak.
            kitapListesi = bookListesi;
            int altLimit = kitapSayac;
            int ustLimit = kitapSayac + 5;
            for (int i = altLimit; i < ustLimit; i++)
            {
                if(i< bookListesi.Count)
                {
                    int mod = i % 5;
                    if (mod == 0)
                    {
                        lbl1Adi.Text = (bookListesi[i]).name;
                        lbl1Yazar.Text = (bookListesi[i]).Author;
                        lbl1Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl1Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl1Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx1.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 1)
                    {
                        lbl2Adi.Text = (bookListesi[i]).name;
                        lbl2Yazar.Text = (bookListesi[i]).Author;
                        lbl2Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl2Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl2Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx2.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 2)
                    {
                        lbl3Adi.Text = (bookListesi[i]).name;
                        lbl3Yazar.Text = (bookListesi[i]).Author;
                        lbl3Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl3Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl3Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx3.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 3)
                    {
                        lbl4Adi.Text = (bookListesi[i]).name;
                        lbl4Yazar.Text = (bookListesi[i]).Author;
                        lbl4Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl4Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl4Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx4.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 4)
                    {
                        lbl5Adi.Text = (bookListesi[i]).name;
                        lbl5Yazar.Text = (bookListesi[i]).Author;
                        lbl5Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl5Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl5Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx5.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    kitapSayac++;
                }
            }
        }
        internal void dergileriGoster(List<Magazine> magazineListesi)
        {
            dergiGeriSayac = dergiSayac;
            tiklanmaDurumu = 2;// dergi göstermek için tıklandığında 2 olacak.
            dergiListesi = magazineListesi;
            int altLimit = dergiSayac;
            int ustLimit = dergiSayac + 5;

            for (int i = altLimit; i < ustLimit; i++)
            {
                if(i<dergiListesi.Count)
                {
                    int mod = i % 5;
                    if (mod == 0)
                    {
                        lbl1Adi.Text = magazineListesi[i].name;
                        lbl1Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl1Yayinci.Text = magazineListesi[i].Issue;
                        lbl1Yazar.Visible = false;
                        lbl1Sayfa.Visible = false;
                        pbx1.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    else if (mod == 1)
                    {
                        lbl2Adi.Text = magazineListesi[i].name;
                        lbl2Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl2Yayinci.Text = magazineListesi[i].Issue;
                        lbl2Yazar.Visible = false;
                        lbl2Sayfa.Visible = false;
                        pbx2.ImageLocation = "dergi" + (i + 1) + ".jpg";

                    }
                    else if (mod == 2)
                    {
                        lbl3Adi.Text = magazineListesi[i].name;
                        lbl3Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl3Yayinci.Text = magazineListesi[i].Issue;
                        lbl3Yazar.Visible = false;
                        lbl3Sayfa.Visible = false;
                        pbx3.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    else if (mod == 3)
                    {
                        lbl4Adi.Text = magazineListesi[i].name;
                        lbl4Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl4Yayinci.Text = magazineListesi[i].Issue;
                        lbl4Yazar.Visible = false;
                        lbl4Sayfa.Visible = false;
                        pbx4.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    else if (mod == 4)
                    {
                        lbl5Adi.Text = magazineListesi[i].name;
                        lbl5Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl5Yayinci.Text = magazineListesi[i].Issue;
                        lbl5Yazar.Visible = false;
                        lbl5Sayfa.Visible = false;
                        pbx5.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    dergiSayac++;
                }
            }
        }
        internal void muzikCDleriGoster(List<MusicCD> musicCDListesi)
        {
            muzikCdGeriSayac = muzikCdSayac;
            tiklanmaDurumu = 3;// music cd göstermek için tıklandığında 3 olacak.
            muzikCdListesi = musicCDListesi;
            int altLimit = muzikCdSayac;
            int ustLimit = muzikCdSayac + 5;

            for (int i = altLimit; i < ustLimit; i++)
            {
                if (i < musicCDListesi.Count)
                {
                    int mod = i % 5;
                    if (mod == 0)
                    {
                        lbl1Adi.Text = musicCDListesi[i].name;
                        lbl1Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl1Yayinci.Text = musicCDListesi[i].Singer;
                        lbl1Yazar.Visible = false;
                        lbl1Sayfa.Visible = false;
                    }
                    else if (mod == 1)
                    {
                        lbl2Adi.Text = musicCDListesi[i].name;
                        lbl2Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl2Yayinci.Text = musicCDListesi[i].Singer;
                        lbl2Yazar.Visible = false;
                        lbl2Sayfa.Visible = false;

                    }
                    else if (mod == 2)
                    {
                        lbl3Adi.Text = musicCDListesi[i].name;
                        lbl3Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl3Yayinci.Text = musicCDListesi[i].Singer;
                        lbl3Yazar.Visible = false;
                        lbl3Sayfa.Visible = false;
                    }
                    else if (mod == 3)
                    {
                        lbl4Adi.Text = musicCDListesi[i].name;
                        lbl4Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl4Yayinci.Text = musicCDListesi[i].Singer;
                        lbl4Yazar.Visible = false;
                        lbl4Sayfa.Visible = false;
                    }
                    else if (mod == 4)
                    {
                        lbl5Adi.Text = musicCDListesi[i].name;
                        lbl5Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl5Yayinci.Text = musicCDListesi[i].Singer;
                        lbl5Yazar.Visible = false;
                        lbl5Sayfa.Visible = false;
                    }
                    muzikCdSayac++;
                }
            }
        }
        ///////
        internal void kitaplariGeriGoster(List<Book> bookListesi)
        {
            kitapSayac = kitapGeriSayac;
            tiklanmaDurumu = 1;// kitap göstermek için tıklandığında 1 olacak.
            kitapListesi = bookListesi;
            int altLimit = kitapGeriSayac-5;
            int ustLimit = kitapGeriSayac;
            for (int i = altLimit; i < ustLimit; i++)
            {
                if (-1<i)
                {
                    int mod = i % 5;
                    if (mod == 0)
                    {
                        lbl1Adi.Text = (bookListesi[i]).name;
                        lbl1Yazar.Text = (bookListesi[i]).Author;
                        lbl1Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl1Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl1Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx1.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 1)
                    {
                        lbl2Adi.Text = (bookListesi[i]).name;
                        lbl2Yazar.Text = (bookListesi[i]).Author;
                        lbl2Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl2Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl2Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx2.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 2)
                    {
                        lbl3Adi.Text = (bookListesi[i]).name;
                        lbl3Yazar.Text = (bookListesi[i]).Author;
                        lbl3Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl3Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl3Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx3.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 3)
                    {
                        lbl4Adi.Text = (bookListesi[i]).name;
                        lbl4Yazar.Text = (bookListesi[i]).Author;
                        lbl4Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl4Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl4Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx4.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    else if (mod == 4)
                    {
                        lbl5Adi.Text = (bookListesi[i]).name;
                        lbl5Yazar.Text = (bookListesi[i]).Author;
                        lbl5Yayinci.Text = (bookListesi[i]).Publisher;
                        lbl5Sayfa.Text = (bookListesi[i]).Page.ToString();
                        lbl5Fiyat.Text = (bookListesi[i]).price.ToString();
                        pbx5.ImageLocation = "kitap" + (i + 1) + ".png";
                    }
                    kitapGeriSayac--;
                }
            }
        }
        internal void dergileriGeriGoster(List<Magazine> magazineListesi)
        {
            dergiSayac = dergiGeriSayac;
            tiklanmaDurumu = 2;// dergi göstermek için tıklandığında 2 olacak.
            dergiListesi = magazineListesi;
            int altLimit = dergiGeriSayac-5;
            int ustLimit = dergiGeriSayac;

            for (int i = altLimit; i < ustLimit; i++)
            {
                if (-1 < i)
                {
                    int mod = i % 5;
                    if (mod == 0)
                    {
                        lbl1Adi.Text = magazineListesi[i].name;
                        lbl1Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl1Yayinci.Text = magazineListesi[i].Issue;
                        lbl1Yazar.Visible = false;
                        lbl1Sayfa.Visible = false;
                        pbx1.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    else if (mod == 1)
                    {
                        lbl2Adi.Text = magazineListesi[i].name;
                        lbl2Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl2Yayinci.Text = magazineListesi[i].Issue;
                        lbl2Yazar.Visible = false;
                        lbl2Sayfa.Visible = false;
                        pbx2.ImageLocation = "dergi" + (i + 1) + ".jpg";

                    }
                    else if (mod == 2)
                    {
                        lbl3Adi.Text = magazineListesi[i].name;
                        lbl3Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl3Yayinci.Text = magazineListesi[i].Issue;
                        lbl3Yazar.Visible = false;
                        lbl3Sayfa.Visible = false;
                        pbx3.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    else if (mod == 3)
                    {
                        lbl4Adi.Text = magazineListesi[i].name;
                        lbl4Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl4Yayinci.Text = magazineListesi[i].Issue;
                        lbl4Yazar.Visible = false;
                        lbl4Sayfa.Visible = false;
                        pbx4.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    else if (mod == 4)
                    {
                        lbl5Adi.Text = magazineListesi[i].name;
                        lbl5Fiyat.Text = magazineListesi[i].price.ToString();
                        lbl5Yayinci.Text = magazineListesi[i].Issue;
                        lbl5Yazar.Visible = false;
                        lbl5Sayfa.Visible = false;
                        pbx5.ImageLocation = "dergi" + (i + 1) + ".jpg";
                    }
                    dergiGeriSayac--;
                }
            }
        }
        internal void muzikCDleriGeriGoster(List<MusicCD> musicCDListesi)
        {
            muzikCdSayac = muzikCdGeriSayac;
            tiklanmaDurumu = 3;// music cd göstermek için tıklandığında 3 olacak.
            muzikCdListesi = musicCDListesi;
            int altLimit = muzikCdGeriSayac-5;
            int ustLimit = muzikCdGeriSayac ;

            for (int i = altLimit; i < ustLimit; i++)
            {
                if (-1 < i)
                {
                    int mod = i % 5;
                    if (mod == 0)
                    {
                        lbl1Adi.Text = musicCDListesi[i].name;
                        lbl1Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl1Yayinci.Text = musicCDListesi[i].Singer;
                        lbl1Yazar.Visible = false;
                        lbl1Sayfa.Visible = false;
                    }
                    else if (mod == 1)
                    {
                        lbl2Adi.Text = musicCDListesi[i].name;
                        lbl2Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl2Yayinci.Text = musicCDListesi[i].Singer;
                        lbl2Yazar.Visible = false;
                        lbl2Sayfa.Visible = false;

                    }
                    else if (mod == 2)
                    {
                        lbl3Adi.Text = musicCDListesi[i].name;
                        lbl3Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl3Yayinci.Text = musicCDListesi[i].Singer;
                        lbl3Yazar.Visible = false;
                        lbl3Sayfa.Visible = false;
                    }
                    else if (mod == 3)
                    {
                        lbl4Adi.Text = musicCDListesi[i].name;
                        lbl4Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl4Yayinci.Text = musicCDListesi[i].Singer;
                        lbl4Yazar.Visible = false;
                        lbl4Sayfa.Visible = false;
                    }
                    else if (mod == 4)
                    {
                        lbl5Adi.Text = musicCDListesi[i].name;
                        lbl5Fiyat.Text = musicCDListesi[i].price.ToString();
                        lbl5Yayinci.Text = musicCDListesi[i].Singer;
                        lbl5Yazar.Visible = false;
                        lbl5Sayfa.Visible = false;
                    }
                    muzikCdGeriSayac--;
                }
            }
        }
        ///////
        private void btnIleriGoster_Click(object sender, EventArgs e)
        {
            if (tiklanmaDurumu == 1)
            {
                kitaplariGoster(kitapListesi);
            }
            else if(tiklanmaDurumu==2)
            {
                dergileriGoster(dergiListesi);
            }
            else if(tiklanmaDurumu==3)
            {
                muzikCDleriGoster(muzikCdListesi);
            }
        }

        private void btnGeriGoster_Click(object sender, EventArgs e)
        {
            if (tiklanmaDurumu == 1)
            {
                kitaplariGeriGoster(kitapListesi);
            }
            else if (tiklanmaDurumu == 2)
            {
                dergileriGeriGoster(dergiListesi);
            }
            else if (tiklanmaDurumu == 3)
            {
                muzikCDleriGeriGoster(muzikCdListesi);
            }
        }
    }
}
