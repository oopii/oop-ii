﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace OOP2_PROJE
{
    class SQLkitapCek:SQL_Veri_Cek
    {
        private SqlConnection bagla = new SqlConnection("Data Source=.;Initial Catalog=BookStore;Integrated Security=true");
        SqlCommand komut;
        SqlDataReader veriOku;

        public SqlDataReader VeriOku { get => veriOku; set => veriOku = value; }
        public SqlCommand Komut { get => komut; set => komut = value; }
        public SqlConnection Bagla { get => bagla; set => bagla = value; }

        Book bookNesnesi = new Book();
        FormAdmin formAdmin_obj = new FormAdmin();
        FormUrünler formUrünler_obj = new FormUrünler();

        public void kitaplariSQLdenCek()
        {
            bagla.Open();
            komut = new SqlCommand("SELECT bookID,bookName,bookPrice,bookAuthor,bookPublisher,bookISBN, bookPage FROM TableBook", bagla);
            VeriOku = komut.ExecuteReader();
            List<Book> kitapListesi = new List<Book>();
            
            while (VeriOku.Read())
            {
                Book nesneTut = new Book();
                bookNesnesi.ID = VeriOku["bookID"].ToString();
                bookNesnesi.name = VeriOku["bookName"].ToString();
                bookNesnesi.price = double.Parse(VeriOku["bookPrice"].ToString());
                bookNesnesi.Author = veriOku["bookAuthor"].ToString();
                bookNesnesi.Publisher = veriOku["bookPublisher"].ToString();
                bookNesnesi.Page = int.Parse(veriOku["bookPage"].ToString());
                bookNesnesi.IsbnNumber = veriOku["bookISBN"].ToString();
                /* verileri nesneTut ile teker teker Book Class turunde tutuyoruz*/
                nesneTut.id = bookNesnesi.id;
                nesneTut.name = bookNesnesi.name;
                nesneTut.price = bookNesnesi.price;
                nesneTut.Author = bookNesnesi.Author;
                nesneTut.Publisher = bookNesnesi.Publisher;
                nesneTut.Page = bookNesnesi.Page;
                nesneTut.IsbnNumber = bookNesnesi.IsbnNumber;
                /*tutulmus olan nesneTut verilerini kitapListesine ekliyoruz*/
                kitapListesi.Add(nesneTut);
            }
            formUrünler_obj.Show();
            bagla.Close();
            formUrünler_obj.kitaplariGoster(kitapListesi);
        }
    }
}
