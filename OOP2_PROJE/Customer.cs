﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_PROJE
{
    class Customer:User
    {
        private static Customer singletonNesnesi=null;

        public string ID { get => id; set => id = value; }
        public String Name { get => name; set => name = value; }
        public String Address { get => address; set => address = value; }
        public String Email { get => email; set => email = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Password { get => password; set => password = value; }

        private Customer()
        {

        }
        public static Customer customer
        {
            get
            {
                if(singletonNesnesi==null)
                {
                    singletonNesnesi = new Customer();
                }
                return singletonNesnesi;
            }
        }

        public void printCustomerDetails()
        {

        }
        public void saveCustomer()
        {

        }
        public void printCustomerPurchases()
        {

        }
    }
}
